FROM openjdk:8u162-jdk-slim-stretch

MAINTAINER XiVO Team "dev@avencall.com"

RUN apt-get update \
    && apt-get install -y --no-install-recommends --auto-remove \
    curl \
    tar \
    wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN curl -O -L -J https://sourceforge.net/projects/jasperstarter/files/JasperStarter-3.5/jasperstarter-3.5.0-bin.tar.bz2/download
RUN tar xj -C /opt -f jasperstarter-3.5.0-bin.tar.bz2

RUN wget https://gitlab.com/xivocc/sample_reports/raw/master/switchboard.jrxml

RUN cd /opt/jasperstarter/jdbc \
    && wget https://jdbc.postgresql.org/download/postgresql-42.2.9.jar

ENV PATH="$PATH:/opt/jasperstarter/bin"

# Version
ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}
