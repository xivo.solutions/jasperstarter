#!/usr/bin/env bash
set -e

if [ -z $TARGET_VERSION ]; then
    echo "TARGET_VERSION is not available"
    exit -1
fi

docker build --no-cache --build-arg TARGET_VERSION=$TARGET_VERSION -t xivoxc/xivo-jasperstarter:$TARGET_VERSION .
